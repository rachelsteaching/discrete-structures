\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures I}
\newcommand{\laTitle}       {Number Theory: Representations of Integers}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 2.6}
\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter 5.2}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

% ASSIGNMENT ------------------------------------ %

    \section*{\laTitle}

    \subsection*{Digits}

        \begin{intro}{\ }
            For the decimal number $2,368$, we can write this as its
            individual digits:

            \begin{center}
                \begin{tabular}{ | c | c | c | c | }
                    \hline
                    \textbf{Thousands}   ($10^{3}$) &
                    \textbf{Hundreds}    ($10^{2}$) &
                    \textbf{Tens}        ($10^{1}$) &
                    \textbf{Ones}        ($10^{0}$)
                    \\ \hline
                    2 & 3 & 6 & 8
                    \\ \hline
                \end{tabular}
            \end{center}

            And then we can build out $2,368$ as the mathematical equation:

            $$ 2 \cdot 10^{3} + 3 \cdot 10^{2} + 6 \cdot 10^{1} + 8 \cdot 10^{0} $$

            Likewise, for the binary number 0101 1001, we can write it as:

            \begin{center}
                \begin{tabular}{ | c | c | c | c | c | c | c | c | }
                    \hline
                    $2^{7}$ &
                    $2^{6}$ &
                    $2^{5}$ &
                    $2^{4}$ &
                    $2^{3}$ &
                    $2^{2}$ &
                    $2^{1}$ &
                    $2^{0}$
                    \\ \hline
                    0 & 1 & 0 & 1 & 1 & 0 & 0 & 1
                    \\ \hline
                \end{tabular}
            \end{center}

            And into the equation:

            $$1 \cdot 2^{6} + 1 \cdot 2^{4} + 1 \cdot 2^{3} + 1 \cdot 2^{0} $$
        \end{intro}

        \begin{intro}{Hexadecimal Digits} \small
            Hexadecimal numbers go from 0 to 15, but the two-digit numbers (10 through 15)
            are usually represented with letters so that you can write out
            a hex number with single characters.

            \begin{center}
                A = 10  \tab    B = 11  \tab    C = 12  \tab    D = 13  \tab    E = 14 \tab F = 15
            \end{center}
        \end{intro}

        \newpage
            
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Expand each of the following numbers as a mathematical
            equation. Make sure to pay attention to the $base$ value.

            \begin{itemize}
                \item[a.]   Write out the equation for $(19)_{10}$
                    \begin{center}
                        \begin{tabular}{ | c | c | }
                            \hline
                            $10^{1}$ & $10^{0}$
                            \\ \hline
                            \solution{1}{} &
                            \solution{9}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}

                \item[b.]   Write out the equation for (0010 1101)$_{2}$ and calculate the decimal equivalent
                            by expanding the equation and calculating the result.
                    \begin{center}
                        \begin{tabular}{ | c | c | c | c | c | c | c | c | }
                            \hline
                            $2^{7}$ &
                            $2^{6}$ &
                            $2^{5}$ &
                            $2^{4}$ &
                            $2^{3}$ &
                            $2^{2}$ &
                            $2^{1}$ &
                            $2^{0}$
                            \\ \hline
                            \solution{0}{} &
                            \solution{0}{} &
                            \solution{1}{} &
                            \solution{0}{} &
                            \solution{1}{} &
                            \solution{1}{} &
                            \solution{0}{} &
                            \solution{1}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}
                    \vspace{2cm}

                \item[c.]   Write out the equation for $(FA6)_{16}$ and calculate the decimal equivalent
                            by expanding the equation and calculating the result.
                    \begin{center}
                        \begin{tabular}{ | c | c | c | }
                            \hline
                            $16^{2}$ &
                            $16^{1}$ &
                            $16^{0}$
                            \\ \hline
                            \solution{F}{} &
                            \solution{A}{} &
                            \solution{6}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}
                    \vspace{2cm}
            \end{itemize}
        \end{questionNOGRADE}
        
        \newpage
        \subsection*{Converting between bases}

            \begin{intro}{Algorithm for converting a decimal number to base $b$:}
                \begin{enumerate}
                    \item   Input a natural number $n$
                    \item   While $n > 0$, do the following:
                        \begin{enumerate}
                            \item   Divide $n$ by $b$ and get a quotient $q$ and remainder $r$.
                            \item   Write $r$ as the next (right-to-left) digit.
                            \item   Replace the value of $n$ with $q$, and repeat.
                        \end{enumerate}
                \end{enumerate}
            \end{intro}

            
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Convert the following between bases:

            \begin{itemize}
                \item[a.]   Convert (35)$_{10}$ to binary (base-2)
                    \tab $n = 35$, $b = 2$
                    \solution{
                        ~\\
                        $35/2 = 17 + 1/2 \tab (a/b = q + r/b)$ \tab[1cm] $q = 17$, $r = 1$ \\
                        $17/2 = 8 + 1/2 $ \tab[5cm] $q = 8$, $r = 1$ \\
                        $8/2 = 4 + 0/2$ \tab[5.2cm] $q = 4$, $r = 0$ \\
                        $4/2 = 2 + 0/2$ \tab[5.2cm] $q = 2$, $r = 0$ \\
                        $2/2 = 1 + 0/2$ \tab[5.2cm] $q = 1$, $r = 0$ \\
                        $1/2 = 0 + 1/2$ \tab[5.2cm] $q = 0$, $r = 1$ \\
                        n = 0 \\ \\
                        = 0010 0011
                    }{ \vspace{5cm} }

                \item[b.]   Convert (125)$_{10}$ to binary (base-2)
                    \tab $n = 125$, $b = 2$
                    \solution{
                        ~\\
                        $125/2 = 62 + 1/2 \tab (a/b = q + r/b)$ \tab[1cm] $q = 62$, $r = 1$ \\
                        $62/2 = 31 + 0/2 $ \tab[5cm] $q = 31$, $r = 0$ \\
                        $31/2 = 15 + 1/2 $ \tab[5cm] $q = 15$, $r = 1$ \\
                        $15/2 = 7 + 1/2 $ \tab[5.2cm] $q = 7$, $r = 1$ \\
                        $7/2 = 3 + 1/2 $ \tab[5.4cm] $q = 3$, $r = 1$ \\
                        $3/2 = 1 + 1/2 $ \tab[5.4cm] $q = 1$, $r = 1$ \\
                        $1/2 = 0 + 1/2 $ \tab[5.4cm] $q = 0$, $r = 1$ \\
                        n = 0 \\ \\
                        = 0111 1101
                    }{ \vspace{3cm} }
            \end{itemize}
        \end{questionNOGRADE}

        \newpage

            \begin{intro}{Hexadecimal to Binary}
                Often in computers, we write binary strings as hexadecimal
                to save space and make it easier to read.

                \begin{center}
                    \begin{tabular}{l c c c c c c c c}
                        Hex & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7
                        \\
                        Binary & 0000 & 0001 & 0010 & 0011 & 0100 & 0101 & 0110 & 0111
                        \\ \hline
                        Hex & 8 & 9 & A & B & C & D & E & F
                        \\
                        Binary & 1000 & 1001 & 1010 & 1011 & 1100 & 1101 & 1110 & 1111
                    \end{tabular}
                \end{center}

                \paragraph{Example:}
                Convert 11001 from binary to hexadecimal \\
                    1. Write out in chunks of four. Add leading 0's to the left side. \\
                        \tab 0001 1001 \\
                    2. Swap out each ``nibble" with hexadecimal \\
                        \tab 0001 = 1 \tab 1001 = 9 \\ \\
                    So, (0001 1001)$_{2}$ = (19)$_{16}$

                \paragraph{Example:}
                Convert $DAD$ from hexadecimal to binary \\
                    1. Convert each digit back to binary. \\
                        \tab D = 1101 \tab A = 1010 \tab D = 1101
                    \\ \\ So, (DAD)$_{16}$ = (1101 1010 1101)$_{2}$
            \end{intro}
            
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Do the following conversions

            \begin{itemize}
                \item[a.]   Convert $(1F0B)_{16}$ to binary:
                    \solution{
                        ~\\
                        1 = 0001 \tab F = 1111 \tab 0 = 0000 \tab B = 1011 \\ \\
                        = 0001 1111 0000 1011
                    }{ \vspace{2cm} }

                \item[b.]   Convert (0100 0110)$_{2}$ to hexadecimal:
                    \solution{
                        ~\\
                        0100 = 4 \tab 0110 = 6 \\ \\
                        = 46
                    }{ \vspace{2cm} }
            \end{itemize}
        \end{questionNOGRADE}

        \newpage

        \begin{intro}{Conversion cheat sheet} ~\\
            \textbf{1. Converting from base $b$ to base 10:}
            Expand the number in base $b$ to its digit-spaces and create an equation.

            \subparagraph{Example: Convert $(121)_{3}$ to base 10}
            \begin{center}
                \begin{tabular}{ | c | c | c | }
                    \hline
                    $3^{2}$ & $3^{1}$ & $3^{0}$
                    \\ \hline
                    1 & 2 & 1
                    \\ \hline
                \end{tabular}

                $$= 1 \cdot 3^{2} + 2 \cdot 3^{1} + 1 \cdot 3^{0}$$
                $$= 9 + 6 + 1$$
                $$= 16$$
            \end{center}

            \paragraph{2. Converting between \underline{Binary} and \underline{Hexadecimal}:}
            For converting between base-2 and base-16 (ONLY) you can use this table to
            swap out 4 bits at a time:
            
                \begin{center}
                    \begin{tabular}{l c c c c c c c c}
                        Hex & 0 & 1 & 2 & 3 & 4 & 5 & 6 & 7
                        \\
                        Binary & 0000 & 0001 & 0010 & 0011 & 0100 & 0101 & 0110 & 0111
                        \\ \hline
                        Hex & 8 & 9 & A & B & C & D & E & F
                        \\
                        Binary & 1000 & 1001 & 1010 & 1011 & 1100 & 1101 & 1110 & 1111
                    \end{tabular}
                \end{center}

                \subparagraph{Example: Convert $(ABC)_{16}$ to binary} ~\\
                $$A = 1010    \tab    B = 1011    \tab    C = 1100$$
                \begin{center}
                    $(ABC)_{16}$ = ( \texttt{1010 1011 1100} )$_{2}$
                \end{center}

                \subparagraph{Example: Convert \texttt{(1111 1010 0010)$_{2}$} to hexadecimal} ~\\
                $$1111 = F      \tab    1010 = A    \tab    0010 = 2$$
                \begin{center}
                    ( \texttt{1111 1010 0010} )$_{2}$ = $(FA2)_{16}$
                \end{center}

            \paragraph{3. Convert decimal to binary:} Use the algorithm!
        \end{intro}

        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Convert each of the following using the appropriate methods.

            \begin{itemize}
                \item[a.]   $(10)_{10}$ to base-2.
                            \vspace{3cm}
                \item[b.]   $(67)_{10}$ to base-2.
                            \vspace{3cm}
                \item[c.]   $($ \texttt{1111 1010 1100 1110} $)_{2}$ to base-16.
                            \vspace{2cm}
                \item[d.]   $($ \texttt{1111 1010 1100 1110} $)_{2}$ to base-10.
                            \vspace{2cm}
                \item[e.]   $(1EE7)_{16}$ to base-2.
                            \vspace{2cm}
                \item[f.]   $(1EE7)_{16}$ to base-10.
                            \vspace{2cm}
            \end{itemize}
        \end{questionNOGRADE}

        

    \newpage

        \notonkey{}{
        \newpage
        \begin{enumerate}
            \item   
            \begin{itemize}
                \item[a.]   Write out the equation for $(19)_{10}$
                    \begin{center}
                        \begin{tabular}{ | c | c | }
                            \hline
                            $10^{1}$ & $10^{0}$
                            \\ \hline
                            \solution{1}{} &
                            \solution{9}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}

                \item[b.]   Write out the equation for (0010 1101)$_{2}$ and calculate the decimal equivalent
                            by expanding the equation and calculating the result.
                    \begin{center}
                        \begin{tabular}{ | c | c | c | c | c | c | c | c | }
                            \hline
                            $2^{7}$ &
                            $2^{6}$ &
                            $2^{5}$ &
                            $2^{4}$ &
                            $2^{3}$ &
                            $2^{2}$ &
                            $2^{1}$ &
                            $2^{0}$
                            \\ \hline
                            \solution{0}{} &
                            \solution{0}{} &
                            \solution{1}{} &
                            \solution{0}{} &
                            \solution{1}{} &
                            \solution{1}{} &
                            \solution{0}{} &
                            \solution{1}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}
                    \vspace{2cm}

                \item[c.]   Write out the equation for $(FA6)_{16}$ and calculate the decimal equivalent
                            by expanding the equation and calculating the result.
                    \begin{center}
                        \begin{tabular}{ | c | c | c | }
                            \hline
                            $16^{2}$ &
                            $16^{1}$ &
                            $16^{0}$
                            \\ \hline
                            \solution{F}{} &
                            \solution{A}{} &
                            \solution{6}{}
                            \\ \hline
                        \end{tabular}
                    \end{center}
                    \vspace{2cm}
            \end{itemize}

            \item
            \begin{itemize}
                \item[a.]   Convert (35)$_{10}$ to binary (base-2)
                    \tab $n = 35$, $b = 2$
                    \solution{
                        ~\\
                        $35/2 = 17 + 1/2 \tab (a/b = q + r/b)$ \tab[1cm] $q = 17$, $r = 1$ \\
                        $17/2 = 8 + 1/2 $ \tab[5cm] $q = 8$, $r = 1$ \\
                        $8/2 = 4 + 0/2$ \tab[5.2cm] $q = 4$, $r = 0$ \\
                        $4/2 = 2 + 0/2$ \tab[5.2cm] $q = 2$, $r = 0$ \\
                        $2/2 = 1 + 0/2$ \tab[5.2cm] $q = 1$, $r = 0$ \\
                        $1/2 = 0 + 1/2$ \tab[5.2cm] $q = 0$, $r = 1$ \\
                        n = 0 \\ \\
                        = 0010 0011
                    }{}

                \item[b.]   Convert (125)$_{10}$ to binary (base-2)
                    \tab $n = 125$, $b = 2$
                    \solution{
                        ~\\
                        $125/2 = 62 + 1/2 \tab (a/b = q + r/b)$ \tab[1cm] $q = 62$, $r = 1$ \\
                        $62/2 = 31 + 0/2 $ \tab[5cm] $q = 31$, $r = 0$ \\
                        $31/2 = 15 + 1/2 $ \tab[5cm] $q = 15$, $r = 1$ \\
                        $15/2 = 7 + 1/2 $ \tab[5.2cm] $q = 7$, $r = 1$ \\
                        $7/2 = 3 + 1/2 $ \tab[5.4cm] $q = 3$, $r = 1$ \\
                        $3/2 = 1 + 1/2 $ \tab[5.4cm] $q = 1$, $r = 1$ \\
                        $1/2 = 0 + 1/2 $ \tab[5.4cm] $q = 0$, $r = 1$ \\
                        n = 0 \\ \\
                        = 0111 1101
                    }{}
            \end{itemize}

            \item
        % - QUESTION --------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}
            Do the following conversions

            \begin{itemize}
                \item[a.]   Convert $(1F0B)_{16}$ to binary:
                    \solution{
                        ~\\
                        1 = 0001 \tab F = 1111 \tab 0 = 0000 \tab B = 1011 \\ \\
                        = 0001 1111 0000 1011
                    }{  }

                \item[b.]   Convert (0100 0110)$_{2}$ to hexadecimal:
                    \solution{
                        ~\\
                        0100 = 4 \tab 0110 = 6 \\ \\
                        = 46
                    }{}
            \end{itemize}
        \end{questionNOGRADE}

        \end{enumerate}
        }

        
\input{../BASE-4-FOOT}
