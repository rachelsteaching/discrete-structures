
    $( document ).ready( function() {
        cabbage = 1, goat = 1, traveller = 1, wolf = 1;
            
        $( "#submit" ).click( function() {
            var choice = $( "#input" ).val();
            console.log( "\"" + choice + "\"" );
            $( "#input" ).val( "" );

            WriteText( "\t" + choice );

            if ( choice == "1" )        MoveObject( "cabbage" );
            else if ( choice == "2" )   MoveObject( "goat" );
            else if ( choice == "3" )   MoveObject( "traveller" );
            else if ( choice == "4" )   MoveObject( "wolf" );

            if ( IsLoseState( cabbage, goat, traveller, wolf ) )
            {
                WriteText( "YOU LOSE! \n" );
                ScrollToBottom();
            }
            else if ( IsWinState( cabbage, goat, traveller, wolf ) )
            {
                WriteText( "YOU WIN! \n" );
                ScrollToBottom();
            }
            else
            {
                WriteText( "\n\n" );
                Main();
            }
        } );

        function ScrollToBottom()
        {
            var gameout = $( "#game" )
            if(gameout.length)
               gameout.scrollTop(gameout[0].scrollHeight - gameout.height());
        }
        
        function WriteText( txt )
        {
            $( "#game" ).append( txt );
        }

        function DisplayState( cabbage, goat, traveller, wolf )
        {
            WriteText( "    " );
            if ( cabbage == 1 )     WriteText( "C" );
            if ( goat == 1 )        WriteText( "G" );
            if ( traveller == 1 )   WriteText( "T" );
            if ( wolf == 1 )        WriteText( "W" );

            WriteText( "\t\t\t    " );
            if ( cabbage == 2 )     WriteText( "C" );
            if ( goat == 2 )        WriteText( "G" );
            if ( traveller == 2 )   WriteText( "T" );
            if ( wolf == 2 )        WriteText( "W" );

            WriteText( "\n" );
            WriteText( " ---------- \t\t ---------- \n" );
            WriteText( "/ ISLAND 1 \\\t\t/ ISLAND 2 \\" );
            WriteText( "\n" );
            WriteText( "\n" );
        }

        function Menu()
        {
            WriteText( "1. move CABBAGE and TRAVELLER    \n" );
            WriteText( "2. move GOAT and TRAVELLER       \n" );
            WriteText( "3. move TRAVELLER ONLY           \n" );
            WriteText( "4. move WOLF and TRAVELLER       \n" );
        }

        function MoveObject( name )
        {
            var object;
            
            if ( name == "cabbage" )        object = cabbage;
            else if ( name == "goat" )      object = goat;
            else if ( name == "traveller" ) object = traveller;
            else if ( name == "wolf" )      object = wolf;
            
            if ( object == traveller )
            {
                if ( object == 1 )     object = 2;
                else                    object = 1;
                traveller = object;
            }
            else
            {
                WriteText( "\t ** Can't move that! Traveller must be in same location! ** \n" );
                return;
            }
            
            if ( name == "cabbage" )        cabbage = object;
            else if ( name == "goat" )      goat = object;
            else if ( name == "traveller" ) traveller = object;
            else if ( name == "wolf" )      wolf = object;
            
            WriteText( "\n Object moved. \n" );
        }

        function IsLoseState( cabbage, goat, traveller, wolf )
        {
            console.log( "IsLoseState", cabbage, goat, traveller, wolf );
            if ( cabbage == goat && cabbage != traveller )
            {
                WriteText( "** GOAT EATS CABBAGE! ** \n" );
                ScrollToBottom();
                return true;
            }
            else if ( goat == wolf && goat != traveller )
            {
                WriteText( "** WOLF EATS GOAT! ** \n" );
                ScrollToBottom();
                return true;
            }

            return false;
        }

        function IsWinState( cabbage, goat, traveller, wolf )
        {
            console.log( "IsWinState", cabbage, goat, traveller, wolf );
            return ( cabbage == 2 && goat == 2 && traveller == 2 && wolf == 2 );
        }

        function Main()
        {
            console.log( "Main()" );
            DisplayState( cabbage, goat, traveller, wolf );
            Menu();

            WriteText( "\n" );
            WriteText( "Choice?" );

            ScrollToBottom();
        }

        $( "#game" ).text( "" );
        Main();
    } );
