\input{../BASE-1-HEAD}
\newcommand{\laClass}       {Discrete Structures II}
\newcommand{\laTitle}       {Graph Theory: Binary Trees}
\newcommand{\laTextbookA}   {Ensley \& Crawley: Chapter 7.6}
%\newcommand{\laTextbookB}   {Johnsonbaugh: Chapter ?}
\newcommand{\laTextbookB}   {}
\addtocounter{section}{-1}
\newcounter{question}

\toggletrue{answerkey}      \togglefalse{answerkey}

\input{../BASE-2-HEADER}
\input{../BASE-3-INSTRUCTIONS-EXERCISE}

    \section{\laTitle}

    \subsection{Intro to Trees}

    \begin{intro}{Terminology} ~\\         
        \textbf{Tree:} A collection of \textbf{Nodes (or vertices)} and \textbf{Edges}.

        \paragraph{Edge:} A path that connects two Nodes together. If we have $N$ nodes,
            then there are $N-1$ edges.

        \paragraph{Nodes:} A vertex in the tree, usually associated with some data.

        \subparagraph{Root Node:} The source Node of the tree; it has no parents.
        Each Tree has one Root Node, usually drawn at the top. All
        other Nodes descend from the Root Node.

        \subparagraph{Leaf Node:} A Node with no children.
    \end{intro}

    \newpage

    \begin{intro}{Terminology} ~\\
        \textbf{Node Family:} We use family terminology to talk about
            how Nodes are related to each other.

            \begin{itemize}
                \item   \textbf{Parent node:} Given some Node $n$, $n$'s parent
                    is the Node immediately above $n$, in the path between $n$
                    and the root node. Each Node can have only 0 or 1 parent.

                \item   \textbf{Ancestor node:} Given some Node $n$, an Ancestor
                    of $n$ is any Node along the path from $n$ to the root node.

                \item   \textbf{Child node:} Given some Node $n$, $n$'s child
                    is a Node that comes immediately below it in the tree.
                    Node $n$ lies in the path from its child to the root node.
                    Each Node can have 0 or more children. With a Binary Search Tree,
                    a Node can have 0, 1, or 2 children.

                \item   \textbf{Descendant node:} Given some Node $n$, a
                    Descendant is a Node that comes below it in the tree, where
                    the Node $n$ lies in the path from that descendant to the root node.

                \item   \textbf{Sibling node:} Given some Node $n$, a Sibling
                    of $n$ is another Node where $n$ and that Sibling share the same
                    Parent node.
            \end{itemize}
    \end{intro}

    \newpage
    
        % -------------------------------------------------------------%
        % - QUESTION --------------------------------------------------%
        % -------------------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            \notonkey{
            For the given tree:

            \begin{center}
                \begin{tikzpicture}
                    \filldraw (0,5) circle (1pt) node[above] {Proto-indo-european};

                    \filldraw (-3,4.5) circle (1pt) node[above] {Slavic};
                    \filldraw (3,4.5) circle (1pt) node[above] {\tab Indo-iranian};
                    \draw (0,5) -- (-3,4.5);
                    \draw (0,5) -- (3,4.5);

                    \filldraw (-5,4) circle (1pt) node[left] {Polish};
                    \filldraw (-3,4) circle (1pt) node[below] {Russian};
                    \filldraw (-1,4) circle (1pt) node[right] {Bulgarian};
                    \draw (-3,4.5) -- (-5,4);
                    \draw (-3,4.5) -- (-3,4);
                    \draw (-3,4.5) -- (-1,4);

                    \filldraw (3,3.5) circle (1pt) node[left] {Persian};
                    \filldraw (5,3.5) circle (1pt) node[right] {Sanskrit};
                    \draw (3,4.5) -- (3,3.5);
                    \draw (3,4.5) -- (5,3.5);

                    \filldraw (4,2.5) circle (1pt) node[below] {Hindi};
                    \filldraw (6,2.5) circle (1pt) node[below] {Urdu};
                    \draw (5,3.5) -- (4,2.5);
                    \draw (5,3.5) -- (6,2.5);

                \end{tikzpicture}
            \end{center}
            }{}

            \begin{itemize}
                \item[a.]   What are all the (listed) ancestors of $Russian$?
                    \solution{ \\ Slavic, Proto-indo-european }{ \vspace{2.5cm} }

                \item[b.]   What are all the (listed) descendants of $Indo-iranian$?
                    \solution{ \\ Persian, Sanskrit, Hindi, Urdu }{ \vspace{2.5cm} }

                \item[c.]   What are all the (listed) siblings of $Polish$?
                    \solution{ \\ Russian and Bulgarian }{ \vspace{2.5cm} }

                \item[d.]   What are all the (listed) leaves of the tree?
                    \solution{ \\ Polish, Russian, Bulgarian, Persian, Hindi, Urdu }{ \vspace{1cm} }
            \end{itemize}

        \end{questionNOGRADE}

        \newpage

    \subsection{Traversals}


    \begin{intro}{Terminology}
        
        \begin{center}
            \begin{tikzpicture}
                \filldraw (0,5) circle (1pt) node[above] {D};
                \filldraw (-2,4) circle (1pt) node[above] {B};
                \filldraw (2,4) circle (1pt) node[above] {F};
                \filldraw (-3,3) circle (1pt) node[below] {A};
                \filldraw (-1,3) circle (1pt) node[below] {C};
                \filldraw (1,3) circle (1pt) node[below] {E};
                \filldraw (3,3) circle (1pt) node[below] {G};

                \draw (0, 5) -- (-2, 4) -- (-3, 3);
                \draw (-2, 4) -- (-1, 3);
                \draw (0, 5) -- (2, 4) -- (1, 3);
                \draw (2, 4) -- (3, 3);
            \end{tikzpicture}
        \end{center}

        \footnotesize

        Since a Tree is not a linear structure, what order do you
        display its contents? There are three main methods you
        will see for traversing through a tree. Each of these
        are recursive, beginning at the root node.
        Once the end of a path is reached (by hitting a leaf),
        the recursion causes it to step back upwards through the tree.


        \paragraph{Pre-order traversal}
            Begin at the Root $r$ node of some Tree/Subtree...

            \begin{enumerate}
                \item   Process $r$
                \item   Traverse left, if available
                \item   Traverse right, if available
            \end{enumerate}

            With the above tree, we process nodes as such: \\ \tab
            \texttt{ D - B - A - C - F - E - G }

        \paragraph{In-order traversal}
            Begin at the Root $r$ node of some Tree/Subtree...

            \begin{enumerate}
                \item   Traverse left, if available
                \item   Process $r$
                \item   Traverse right, if available
            \end{enumerate}

            With the above tree, we process nodes as such: \\ \tab
            \texttt{ A - B - C - D - E - F - G }

        \paragraph{Post-order traversal}
            Begin at the Root $r$ node of some Tree/Subtree...

            \begin{enumerate}
                \item   Traverse left, if available
                \item   Traverse right, if available
                \item   Process $r$
            \end{enumerate}

            With the above tree, we process nodes as such: \\ \tab
            \texttt{ A - C - B - E - G - F - D }

    \end{intro}

    \newpage

    \begin{center}
        \begin{tikzpicture}
            \coordinate (A) at (0,3);
            
            \coordinate (B) at (-1,2);
            \coordinate (C) at (1,2);
            
            \coordinate (D) at (-2,1);
            \coordinate (E) at (0,1);

            \draw (D) -- (B) -- (E);
            \draw (B) -- (A) -- (C);
            
            \filldraw[black,fill=white] (A) circle (10pt) node {A};
            
            \filldraw[black,fill=white] (B) circle (10pt) node {B};
            \filldraw[black,fill=white] (C) circle (10pt) node {C};
            
            \filldraw[black,fill=white] (D) circle (10pt) node {D};
            \filldraw[black,fill=white] (E) circle (10pt) node {E};
        \end{tikzpicture}
    \end{center}
    
    \begin{intro}{Preorder example}
        \footnotesize
        \begin{enumerate}
            \item   Node A: Display ``A'', traverse left.
            \item   Node B: Display ``B'', traverse left.
            \item   Node D: Display ``D'', no left child, no right child, return.
            \item   Node B: Traverse right.
            \item   Node E: Display ``E'', no left child, no right child, return.
            \item   Node B: Done, return
            \item   Node A: Traverse right.
            \item   Node C: Display ``C'', no left child, no right child, return.
            \item   Done. Result: ``ABDEC''
        \end{enumerate}
    \end{intro}
    
    \begin{intro}{Inorder example}
        \footnotesize
        \begin{enumerate}
            \item   Node A: Have left child, traverse left.
            \item   Node B: Have left child, traverse left.
            \item   Node D: No left child, display ``D'', no right child, return.
            \item   Node B: Left child done, display ``B'', traverse right.
            \item   Node E: No left child, display ``E'', no right child, return.
            \item   Node B: Right child done, return.
            \item   Node A: Left child done, display ``A'', traverse right.
            \item   Node C: No left child, display ``C'', no right child, return.
            \item   Done. Result: ``DBEAC''
        \end{enumerate}
    \end{intro}

    \normalsize
    \newpage
    
    \begin{center}
        \begin{tikzpicture}
            \coordinate (A) at (0,3);
            
            \coordinate (B) at (-1,2);
            \coordinate (C) at (1,2);
            
            \coordinate (D) at (-2,1);
            \coordinate (E) at (0,1);

            \draw (D) -- (B) -- (E);
            \draw (B) -- (A) -- (C);
            
            \filldraw[black,fill=white] (A) circle (10pt) node {A};
            
            \filldraw[black,fill=white] (B) circle (10pt) node {B};
            \filldraw[black,fill=white] (C) circle (10pt) node {C};
            
            \filldraw[black,fill=white] (D) circle (10pt) node {D};
            \filldraw[black,fill=white] (E) circle (10pt) node {E};
        \end{tikzpicture}
    \end{center}
        
    \begin{intro}{Postorder example}
        \begin{enumerate}
            \item   Node A: Have left child, traverse left.
            \item   Node B: Have left child, traverse left.
            \item   Node D: No left child, no right child, display ``D'', return.
            \item   Node B: Have right child, traverse right.
            \item   Node E: No left child, no right child, display ``E'', return.
            \item   Node B: Display ``B'', return.
            \item   Node A: Have right child, traverse right.
            \item   Node C: No left child, no right child, display ``C'', return.
            \item   Node A: Display ``A''.
        \end{enumerate}
    \end{intro}

    \newpage

        % -------------------------------------------------------------%
        % - QUESTION --------------------------------------------------%
        % -------------------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Traverse the following tree using \textbf{pre-order} traversal. Write
            out each Node as you ``process" it.
            \notonkey{
            ~\\
            \begin{tikzpicture}
                \draw (0, 5) -- (-1,4);
                \draw (0, 5) -- (1,4);
                \draw (-1,4) -- (-2,3);
                \draw (1,4) -- (2,3);
                \draw (1,4) -- (0.5,3);
                \draw (2,3) -- (1,2);
                \draw (2,3) -- (3,2);

                \draw[fill=white]
                    (0,5)  circle (7pt) node {H};
                \draw[fill=white]
                    (-1,4) circle (7pt) node {I};
                \draw[fill=white]
                    (-2,3) circle (7pt) node {W};
                \draw[fill=white]
                    (1,4)  circle (7pt) node {U};
                \draw[fill=white]
                    (0.5,3)  circle (7pt) node {T};
                \draw[fill=white]
                    (2,3)  circle (7pt) node {S};
                \draw[fill=white]
                    (1,2)  circle (7pt) node {U};
                \draw[fill=white]
                    (3,2)  circle (7pt) node {P};
            \end{tikzpicture}
            }{}
            \solution{
                H I W U T S U P
            }{ }

        \end{questionNOGRADE}

        \hrulefill



        % -------------------------------------------------------------%
        % - QUESTION --------------------------------------------------%
        % -------------------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Traverse the following tree using \textbf{post-order} traversal. Write
            out each Node as you ``process" it.

            ~\\

            \begin{tikzpicture}
                \draw (0, 5) -- (-1,4);
                \draw (0, 5) -- (1,4);
                \draw (-1,4) -- (-2,3);
                \draw (-1,4) -- (-0.5,3);
                \draw (1,4) -- (2,3);
                \draw (1,4) -- (0.5,3);
                \draw (2,3) -- (1,2);
                \draw (2,3) -- (3,2);

                \draw[fill=white]
                    (0,5)  circle (7pt) node {D};
                \draw[fill=white]
                    (-1,4) circle (7pt) node {T};
                \draw[fill=white]
                    (1,4)  circle (7pt) node {O};
                \draw[fill=white]
                    (-2,3) circle (7pt) node {C};
                \draw[fill=white]
                    (-0.5,3) circle (7pt) node {A};
                \draw[fill=white]
                    (0.5,3)  circle (7pt) node {S};
                \draw[fill=white]
                    (2,3)  circle (7pt) node {O};
                \draw[fill=white]
                    (1,2)  circle (7pt) node {R};
                \draw[fill=white]
                    (3,2)  circle (7pt) node {G};
            \end{tikzpicture}

            \solution{
                C A T S R G O O D
            }{  }

        \end{questionNOGRADE}

        \hrulefill

        % -------------------------------------------------------------%
        % - QUESTION --------------------------------------------------%
        % -------------------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            Traverse the following tree using \textbf{in-order} traversal. Write
            out each Node as you ``process" it.

~\\

            \begin{tikzpicture}
                \draw (0, 5) -- (-1,4);
                \draw (0, 5) -- (1,4);
                \draw (-1,4) -- (-2,3);
                \draw (-1,4) -- (-0.5,3);
                \draw (1,4) -- (0.5,3);

                \draw[fill=white]
                    (0,5)  circle (7pt) node {P};
                \draw[fill=white]
                    (-1,4) circle (7pt) node {R};
                \draw[fill=white]
                    (1,4)  circle (7pt) node {S};
                \draw[fill=white]
                    (-2,3) circle (7pt) node {G};
                \draw[fill=white]
                    (-0.5,3) circle (7pt) node {A};
                \draw[fill=white]
                    (0.5,3)  circle (7pt) node {H};
            \end{tikzpicture}

            \solution{
                G R A P H S
            }{  }

        \end{questionNOGRADE}

    \newpage

        % -------------------------------------------------------------%
        % - QUESTION --------------------------------------------------%
        % -------------------------------------------------------------%
        \stepcounter{question}
        \begin{questionNOGRADE}{\thequestion}

            \small
            A \textbf{binary search tree} is a type of tree where each node
            can have 0, 1, or 2 children, but no more than 2.
            For any Node $n$, any nodes to the \textbf{left} of $n$ are less than $n$.
            Similarly, any nodes to the \textbf{right} of $n$ are greater than $n$.

            ~\\
            When the first node is added to a binary tree, it becomes the \textbf{root}.
            When subsequent nodes are added, we traverse the tree, moving to the left
            or right until we find an available space.

            ~\\
            For the following, there is a list of nodes in the order added to a tree.
            Draw out the binary tree once all nodes are added.

            \normalsize

            \paragraph{Example:}
                Add: \texttt{C, B, D, A} ~\\
                
                \begin{tikzpicture}
                    \draw (0,3) -- (-1,2) -- (-2,1);
                    \draw (0,3) -- (1,2);

                    \filldraw[black,fill=white] (0,3) circle (10pt) node {C};
                    \filldraw[black,fill=white] (-1,2) circle (10pt) node {B};
                    \filldraw[black,fill=white] (1,2) circle (10pt) node {D};
                    \filldraw[black,fill=white] (-2,1) circle (10pt) node {A};
                \end{tikzpicture}

            \begin{itemize}
                \item[a.]   Add: \texttt{B, A, D, C, F, E}
                    \solution{
                    \begin{tikzpicture}
                        \draw (-2,-1) -- (0,0) -- (2,-1);
                        \draw (2,-3) -- (3,-2);
                        \draw (3,-2) -- (2,-1) -- (1,-2);
                        \filldraw[fill=white] (0,0) circle (10pt) node{B};
                        \filldraw[fill=white] (-2,-1) circle (10pt) node{A};
                        \filldraw[fill=white] (2,-1) circle (10pt) node{D};
                        \filldraw[fill=white] (1,-2) circle (10pt) node{C};
                        \filldraw[fill=white] (3,-2) circle (10pt) node{F};
                        \filldraw[fill=white] (2,-3) circle (10pt) node{E};
                    \end{tikzpicture}
                    }{ }

                \item[b.]   Add: \texttt{A, B, C, D, E}
                    \solution{
                    \begin{tikzpicture}
                        \draw (0,0) -- (4,-2);
                        \filldraw[fill=white] (0,0) circle (10pt) node{A};
                        \filldraw[fill=white] (1,-0.5) circle (10pt) node{B};
                        \filldraw[fill=white] (2,-1) circle (10pt) node{C};
                        \filldraw[fill=white] (3,-1.5) circle (10pt) node{D};
                        \filldraw[fill=white] (4,-2) circle (10pt) node{E};
                    \end{tikzpicture}
                    }{ }

                \item[c.]   Add: \texttt{E, D, C, B, A}
                    \solution{
                    \begin{tikzpicture}
                        \draw (0,0) -- (-4,-2);

                        \filldraw[fill=white] (0,0) circle (10pt) node{E};
                        \filldraw[fill=white] (-1,-0.5) circle (10pt) node{D};
                        \filldraw[fill=white] (-2,-1) circle (10pt) node{C};
                        \filldraw[fill=white] (-3,-1.5) circle (10pt) node{B};
                        \filldraw[fill=white] (-4,-2) circle (10pt) node{A};
                    \end{tikzpicture}
                    }{ }

            \end{itemize}

        \end{questionNOGRADE}

    \newpage










\input{../BASE-4-FOOT}
