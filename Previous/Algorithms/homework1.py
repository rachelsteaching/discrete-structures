def GetMax( a, b ):
    if ( a > b ):   return a
    else:           return b

def GetMin( a, b ):
    if ( a < b ):   return a
    else:           return b

def GetListMax( myList ):
    maxVal = myList[0]

    for i in range( 1, len( myList ) ):
        if myList[i] > maxVal:
            maxVal = myList[i]

    return maxVal

def GetOrder( x, y ):
    if( x < y ):    return -1
    elif ( x > y ): return 1
    else:           return 0

def Sum( myList ):
    sumVal = 0

    for num in myList:
        sumVal += num

    return sumVal


def AbsVal( num ):
    if ( num < 0 ): return -num
    else:           return num

def Factorial( n ):
    product = 1

    for i in range( n, 1, -1 ):
        product *= i

    return product
    
def FactorialR( n ):
    if n == 0:    return 1
    else:         return n * Factorial( n - 1 )











    
